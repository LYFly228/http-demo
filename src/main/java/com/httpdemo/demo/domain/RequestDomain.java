package com.httpdemo.demo.domain;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Cirilla
 * @projectName http-demo
 * @description:
 * @date 2020/9/22  9:48
 */
@Data
@XmlRootElement(name = "request")
public class RequestDomain {

    private Long id;

    private String name;

    private Integer code;

    private String content;
}
