package com.httpdemo.demo.util;

import cn.hutool.core.util.XmlUtil;
import com.httpdemo.demo.domain.RequestDomain;
import org.w3c.dom.Document;

import java.io.ByteArrayOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * @author Cirilla
 * @projectName http-demo
 * @description:
 * @date 2020/9/21  17:05
 */
public class HttpUtils {

    private static final String url = "http://localhost:8080/post";

    public static String doGet(String url) throws Exception {
        RequestDomain requestDomain = new RequestDomain();
        requestDomain.setId(1L);
        requestDomain.setName("请求方");
        requestDomain.setCode(200);
        requestDomain.setContent("request 发送请求");
        Document document = XmlUtil.beanToXml(requestDomain);
        String format = XmlUtil.format(document);
        // 加密xml
        String encode = TripleDESUtils.encode(format);

        URL url1 = new URL(url);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url1.openConnection();
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setRequestProperty("Content-type", "application/x-java-serialized-object");
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.connect();
        GZIPOutputStream zos = new GZIPOutputStream(httpURLConnection.getOutputStream());
        zos.write(encode.getBytes(), 0, encode.getBytes().length);
        zos.finish();
        zos.close();
        GZIPInputStream zis = new GZIPInputStream(httpURLConnection.getInputStream());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[1024];
        int len = 0;
        while ((len = zis.read(buf)) != -1) {
            baos.write(buf, 0, len);
        }
        zis.close();
        byte[] data = baos.toByteArray();
        baos.close();
        String response = new String(data);
        String decode = TripleDESUtils.decode(response);
        System.out.println(decode);

        return decode;


    }

    public static void main(String[] args) throws Exception {
        String xml = doGet(url);
    }
}
