package com.httpdemo.demo;

import com.httpdemo.demo.util.HttpUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest()
class DemoApplicationTests {

    private static final String url = "http://localhost:8080/post";

    @Test
    public void testRequest() throws Exception {
        String s = HttpUtils.doGet(url);
        System.out.println(s);
    }

}
